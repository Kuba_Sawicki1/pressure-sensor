#ifndef CONFIG_H_
#define CONFIG_H_

struct pressureConfig {
    int pressure_pin = 35;
    float refresh_rate = 100;  // in milliseconds
    float max_pressure = 100;   // max pressure range (currently 1450psi)
    float min_pressure = 0;     // min pressure range
    float max_value = 2482; // max ADC value
    float min_value = 496;    // min ADC value
};

#endif