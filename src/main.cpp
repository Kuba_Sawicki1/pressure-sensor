#include <Arduino.h>
#include "./config.h"

pressureConfig config;

void setup() {
  Serial.begin(115200);

  pinMode(config.pressure_pin, INPUT);
}

void loop() {
  static float analog_reading, pressure_reading = 0;

  float current_time = millis();
  static float old_time = millis();

  if(current_time - old_time > config.refresh_rate) {

    analog_reading = analogRead(config.pressure_pin);

    pressure_reading = (analog_reading-config.min_value)*(config.max_pressure-config.min_pressure)/(config.max_value-config.min_value)+config.min_pressure;
    
    Serial.print("Analog value reading: ");
    Serial.print(analog_reading);
    Serial.print("    Pressure value: ");
    Serial.println(pressure_reading);

    old_time = current_time;
  }
}